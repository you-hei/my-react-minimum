'use strict'

const path = require('path')

module.exports = {
    mode: 'production',
    entry: {
        index: './src/index.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            { test: /\.css$/, use: ['css-loader', 'style-loader'] },
            { test: /\.scss$/, use: ['css-loader', 'style-loader', 'sass-loader'] }
        ]
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public')
    }
}
